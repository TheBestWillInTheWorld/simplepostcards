


function getBase64ImageFromURL(uRL,callback){    
	var sourceImg=new Image(); 
    sourceImg.setAttribute('src', uRL);
	return getBase64Image(sourceImg,callback);
}

function getBase64Image(sourceImg,callback){    
    var dataURL;
	var base64Image;
		
	 var processImage= function () {
		console.log('processImage');	
		if (typeof sourceImg === 'undefined' || sourceImg.naturalWidth !== 0 ) {
			var canvas = document.createElement("canvas");	
			canvas.width = sourceImg.width; 
			canvas.height = sourceImg.height;	
			   
			var context2D = canvas.getContext("2d"); 
			context2D.drawImage(sourceImg, 0, 0); 
			
			dataURL = canvas.toDataURL("image/png");
			base64Image = dataURL.split('base64,')[1];
			console.log("from getbase64 function ");//+base64Image); 
			
			//attempt a cleanup!
			dataURL = null;
			context2D = null;
			canvas = null;
			//call back
			callback(base64Image);
		};
	};
	sourceImg.onload = processImage;
	//TODO: resolve the lack of callback if image has already fully loaded by the time this call is made
	//		also, there's a problem with img vs new Image() vs Jquery selector here!

	processImage();
} 

var showImage = function(imgSrc){
		console.log(imgSrc);
		var imgPreview = $('#imgPreview');
		if (imgPreview.length<=0){
			imgPreview = $("<img>",{
				id	 	: 'imgPreview',
				src	 	: imgSrc,
				style	: 'float:right',
				onload	: function(){console.log('loaded image');} 
			}).appendTo($('#divImage'));
		};
		console.log();
		$(imgPreview).attr("src", imgSrc);
	};
	
var readURL = function(input) {
	var reader = new FileReader();

	reader.onload = function (e) {
		console.log('input file reader loaded');
		showImage(e.target.result);
	}

	reader.readAsDataURL(input.files[0]);
}


