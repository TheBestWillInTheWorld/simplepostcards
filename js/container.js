//requires targetversion.js
var mailingGUID;

//docmail API variables
var accountType = 'Topup';

$(document).ready(function () {	
	$('#domain').append(location.protocol+'//'+document.domain);
	$('#receiver').attr('src', iFrameTarget);
});

//incoming PostMessage should be handled by the individual page
// window.addEventListener('message', function(e) {
                    // $('#result').empty();
					// if (e.data.MailingGUID!=undefined)	mailingGUID=e.data.MailingGUID;
					// $('#result').append('got callback: ' + JSON.stringify(e.data) );
// });


var getBalance = function (){	
					$('#result').empty();
					var receiver = $('#receiver')[0].contentWindow;
					var data = {	'method'	  : 'getBalance',
									'user'		  : $('#txtUsr').val(),
									'password'	  : $('#txtPwd').val(),
									'accountType' : accountType
								};			
					console.log('sending');
					receiver.postMessage(data,targetDomain);
				};

var createMailing = function (){	
					$('#result').empty();
					var receiver = $('#receiver')[0].contentWindow;
					var data = {	'method'	  : 'createMailing',
									'user'		  : $('#txtUsr').val(),
									'password'	  : $('#txtPwd').val()
								};			
					console.log('sending');
					receiver.postMessage(data,targetDomain);
				};

var addDesignerTemplate = function (){
					$('#result').empty();	
					var receiver = $('#receiver')[0].contentWindow;
					var data = {	'method'	  : 'addDesignerTemplate',
									'user'		  : $('#txtUsr').val(),
									'password'	  : $('#txtPwd').val(),
									'mailingGUID' : mailingGUID
								};			
					console.log('sending');
					receiver.postMessage(data,targetDomain);
				};

var addDesignerImage = function (){	
					$('#result').empty();
					console.log('addDesignerImage');
					var receiver = $('#receiver')[0].contentWindow;			
					var data;
					var imageUrl='imagename.jpg';//TODO: get filename from upload control

					//have to define callback in case we're loading an image (async)
					var makeTheCall = function (base64Image){
						console.log('makeTheCall');
						//console.log(base64Image);
						data = {	'method'	      : 'addDesignerImage',
									'user'		      : $('#txtUsr').val(),
									'password'	      : $('#txtPwd').val(),
									'mailingGUID'     : mailingGUID,
									'partDisplayName' : 'Background photo',
									'imageName'		  : imageUrl, 
									'imageUrl'		  : imageUrl,
									'imageData'		  : base64Image,
									'imageRotation'	  : imageRotationOverride,
									'imageFitOption'  : 'Crop'
								};			
						console.log('sending');
						receiver.postMessage(data,targetDomain);
					};
					
					if (base64File()=="") getFileData($('#imgInput'));
					makeTheCall(base64File());
				};

var addDesignerText = function (){	
					$('#result').empty();
					console.log('********* addDesignerText');
					var receiver = $('#receiver')[0].contentWindow;
					var data = {	'method'	  		: 'addDesignerText',
									'user'		  		: $('#txtUsr').val(),
									'password'	  		: $('#txtPwd').val(),
									'mailingGUID' 		: mailingGUID,
									'partDisplayName'	: 'Message',
									'textContent'		: $('#txtMessage').val(),
									'fontSize'			: 20,
									'fontName'			: 'Verdana',
									'bold'				: '0',
									'italic'			: '0',
									'underline'			: '0',
									'textJustification' : 'Center',
									'fontColourRed'		: 0,
									'fontColourGreen'	: 0,
									'fontColourBlue'	: 0
								};			
					console.log('sending');
					receiver.postMessage(data,targetDomain);
				};

var addAddress = function (){	
					$('#result').empty();
					var receiver = $('#receiver')[0].contentWindow;
					var data = {	'method'	  : 'addAddress',
									'user'		  : $('#txtUsr').val(),
									'password'	  : $('#txtPwd').val(),
									'mailingGUID' : mailingGUID,
									'fullName'	  : $('#txtName').val(),
									'addressBlock': $('#txtAddress').val()
								};			
					console.log('sending');
					receiver.postMessage(data,targetDomain);
				};

var processMailing = function (){	
					$('#result').empty();
					var receiver = $('#receiver')[0].contentWindow;
					var data = {	'method'	  : 'processMailing',
									'user'		  : $('#txtUsr').val(),
									'password'	  : $('#txtPwd').val(),
									'mailingGUID' : mailingGUID
								};			
					console.log('sending');
					receiver.postMessage(data,targetDomain);
				};