
var imageRotationOverride	= 0;
var fileDataUrl				= "";


function getBase64ImageFromURL(uRL,callback){    
	var sourceImg=new Image(); 
    sourceImg.setAttribute('src', uRL);
	return getBase64Image(sourceImg,callback);
}

function getBase64Image(sourceImg,callback){    
    var dataURL;
	var base64Image;
		
	 var processImage= function () {
		console.log('processImage');	
		if (typeof sourceImg === 'undefined' || sourceImg.naturalWidth !== 0 ) {
			var canvas = document.createElement("canvas");	
			canvas.width = sourceImg.width; 
			canvas.height = sourceImg.height;		
			console.log(sourceImg);
			   
			var context2D = canvas.getContext("2d"); 
			context2D.drawImage(sourceImg, 0, 0); 
			context2D = null;
			
			dataURL = canvas.toDataURL("image/png");
			canvas = null;
			base64Image = dataURL.split('base64,')[1];
			//console.log("from getbase64 function "+base64Image); 	
			
			console.log('image cleanup');
			//attempt a cleanup!
			dataURL = null;
			//call back
			callback(base64Image);
		};
	};
	sourceImg.onload = processImage;

	processImage();
} 

var showImage = function(imgSrc){
		//console.log(imgSrc);
		var imgHolder = $('#imgHolder');
		var imgPreviewL = $('#imgPreviewL');
		var imgPreviewP = $('#imgPreviewP');
	
		var setupPreview = function(){
			console.log('setup preview');
			console.log(imgPreviewL.height());
			console.log(imgPreviewL.width());
			if (imgPreviewL.height()>imgPreviewL.width()) {
				console.log('load rotated portrait preview'); 
				$(imgPreviewP).attr("src", imgSrc);
				imageRotationOverride = 270;
				imgPreviewL.css('display','none');
				imgPreviewP.css('display','inline');
			}
			else{
				console.log('load rotated landscape preview'); 
				imageRotationOverride = 0;
				imgPreviewL.css('display','inline');
				imgPreviewP.css('display','none');
			}
			console.log('loaded image preview');
		}			
	
		console.log('loading preview');
		if (imgPreviewL.length<=0){
			imgPreviewL = $("<img>",{
				id	 	: 'imgPreviewL',
				src	 	: imgSrc,
				onload	: setupPreview 
			}).appendTo($('#divImage'));
		}
		else {
			$(imgPreviewL).attr("src", imgSrc);
			setupPreview();
		};
				
	};

var base64File = function(){
	return fileDataUrl.split('base64,')[1];
}
	
var loadData = function(input) {
	console.log('loadData');
	getFileData(input,showImage);
}
	
var getFileData = function(input,callback) {
	var reader = new FileReader();

	reader.onload = function (e) {
		console.log('input file reader loaded');
		fileDataUrl = e.target.result;
		if (callback) callback(fileDataUrl);
	}
	reader.readAsDataURL(input.files[0]);
}


